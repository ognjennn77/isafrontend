import React from 'react';
import Home from './pages/Home';
import Registration from './pages/Registration';
import UserConfirm from "./pages/UserConfirm";
import HomePatient from './pages/HomePatient';
import Cartons from './pages/Cartons';
import Schedule from './pages/Schedule';

import { Route } from 'react-router-dom';
import { isUserLoggedIn } from "./base/OAuth";
import Examination from './pages/Examination';
import ExaminationConfirm from './pages/ExaminationConfirm';
import PredefinedExamination from './pages/PredefinedExamination';
import MyProfile from './pages/MyProfile';

let ROUTES = {
    Home: {
        path: '/',
        component: <Home/>,
        auth: false
    },
    Registration: {
        path: '/registration',
        component: <Registration/>,
        auth: false
    },
    UserConfirm: {
        path: '/user/confirm',
        component: <UserConfirm/>,
        auth: false
    },
    HomePatient: {
        path: '/home',
        component: <HomePatient/>,
        auth: false
    },
    Cartons: {
        path: '/cartons',
        component: <Cartons/>,
        auth: false
    },
    Schedule: {
        path: '/schedule',
        component: <Schedule/>,
        auth: false
    },
    Examination: {
        path: '/examinations',
        component: <Examination/>,
        auth: false
    },
    ExaminationConfirm: {
        path: '/examination/confirm',
        component: <ExaminationConfirm/>,
        auth: false
    },
    PredefinedExamination: {
        path: '/predefined-examination',
        component: <PredefinedExamination/>,
        auth: true
    },
    MyProfile: {
        path: '/my-profile',
        component: <MyProfile/>,
        auth: false
    }
};

export default ROUTES;

function getRoute(path) {

    for(const [key, value] of Object.entries(ROUTES)) {

        if(value.path === path) {
            return value;
        }
    }

    return null;
}

export function checkPath(path) {

    let pathObject = getRoute(path);

    if(!pathObject) {
        return true;
    }

    if(pathObject.auth) {
        return !isUserLoggedIn();
    }

    return false;
}

export function getRoutes() {

    let result = [];

    for(const [key, value] of Object.entries(ROUTES)) {

        result.push(
            <Route key={ 'route-' + result.length } exact path={ value.path } render={() => (
                value.component
            )}/>
        )
    }

    return result;
}