import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import { withRouter } from "react-router-dom";

import * as Actions from "../actions/Actions";
import {logout} from "../base/OAuth";


class Header extends Component {

    constructor(props) {
        super(props);
    }


    logout() {
        logout();
        this.props.logout();
        this.props.history.push('/');
    }

    render() {

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="navbar navbar-default navbar-fixed-top">
                        <header id="header-site">
                        
                            <div className="logo-site">
                                <h1><a href="#">Leistung</a></h1>
                            </div>
                            
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span className="sr-only">Toggle navigation</span> 
                                <span className="icon-bar"></span> 
                                <span className="icon-bar"></span> 
                                <span className="icon-bar"></span> 
                            </button>
                            
                           {/*  <ul className="nav navbar-right pull-right top-nav">
                                <li className="dropdown dropdown-notification"> <a class="dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true"> <i class="fa fa-bell-o"></i> <span class="badge badge-default"> 3 </span> </a>
                                    <ul className="dropdown-menu">
                                        <li className="external">
                                            <h3> <span className="bold">12 pending</span> notifications</h3>
                                            <a href="page_user_profile_1.html">view all</a> 
                                        </li>
                                    </ul>
                                </li>
                                <li className="dropdown"> 
                                    <ul className="dropdown-menu">
                                        <li><a href="#"><i className="fa fa-fw fa-user"></i> Edit Profile</a></li>
                                        <li><a href="#"><i className="fa fa-fw fa-cog"></i> Change Password</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#"><i className="fa fa-fw fa-power-off"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul> */}


                            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul className="nav navbar-nav navbar-left">
                                    <li><a id="home" href="/home">Home</a></li>
                                    <li><a id="about" href="/cartons">Karton</a></li>
                                    <li><a id="" href="/examinations">Istorija pregleda</a></li>
                                    <li><a id="" href="/schedule">Zakazani pregledi</a></li>
                                    <li><a id="" href="/predefined-examination">Predefinisani pregledi</a></li>
                                    <li><a id="" href="/my-profile">Moj profile</a></li>
                                    <li><a id="" href="" onClick={() => this.logout()}>Odjava</a></li>
                                </ul>
                            </div>
                            
                        </header>
                    </div>
                </div>
            </div> 
        );
    }
}


function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        logout: Actions.logout
    }, dispatch);
}

function mapStateToProps({ menuReducers, authReducers, siteDataReducers })
{
    return { menu: menuReducers, user: authReducers.user, siteData: siteDataReducers };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));