export const TOGGLE_LOADER = '[SITE_DATA] TOGGLE_LOADER';
export const FORCE_HIDE_LOADER = '[SITE_DATA] FORCE_HIDE_LOADER';
export const GET_GIRL_ID = '[SITE_DATA] GET_GIRL_ID';
export const CLEAR_GIRL_ID = '[SITE_DATA] CLEAR_GIRL_ID';

export function showLoader() {

    return {
        type: TOGGLE_LOADER,
        loader: true
    };
}

export function forceHideLoader() {
    return {
        type: TOGGLE_LOADER
    };
}

export function hideLoader() {
    return {
        type: TOGGLE_LOADER,
        loader: false
    };
}

export function getGirlId(girlId, event) {
    return {
        type: GET_GIRL_ID,
        girlId: girlId,
        event : event
    };
}

export function clearGirlId() {
    return {
        type: CLEAR_GIRL_ID,
        girlId: false,
        event: undefined
    };
}