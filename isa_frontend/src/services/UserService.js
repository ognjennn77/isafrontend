import HttpMethod from "../constants/HttpMethod";
import {request, requestFile} from "../base/HTTP";


export async function register(data) {
    return await request('/api/user/register', data, HttpMethod.POST);
}

export async function getDoctors(data) {
    return await request('/api/get_doctors', data, HttpMethod.GET);
}

export async function getClinics(data) {
    return await request('/api/get_clinics', data, HttpMethod.GET);
}

export async function getExaminations(data) {
    return await request('/api/get_examination', data, HttpMethod.POST);
}

export async function createExamination(data){
    return await request('/api/create_examination', data, HttpMethod.POST);
}

export async function getUserExaminations(data){
    return await request('/api/get_user_examinations', data, HttpMethod.GET);
}

export async function confirmUser(data) {
    return await request('/api/user/confirm', data, HttpMethod.POST);
}

export async function confirmEximantion(data) {
    return await request('/api/examination/confirm', data, HttpMethod.POST);
}

export async function getPredefinedExamination(){
    return await request('/api/get_predefined_examinations', [], HttpMethod.GET);
}

export async function changeExaminationStatus(data){
    return await request('/api/change_examination_status', data, HttpMethod.PUT);
}

export async function getUser(){
    return await request('/api/get_user', [], HttpMethod.GET);
}