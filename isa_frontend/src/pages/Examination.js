import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";
import { getUserExaminations } from "../services/UserService";
import { getExaminationTypeString } from "../constants/ExaminationType";
import { getExaminationStatusString } from "../constants/ExaminationStatus";
import ExaminationSortType from "../constants/ExaminationSortType";
import moment from "moment";
import SelectControl from "../components/controls/SelectControl";
import update from 'immutability-helper';


class Examination extends Page {

    validationList = {
        email: [ {type: Validators.REQUIRED } ],
        password: [ {type: Validators.REQUIRED } ]
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {
                sort: {value: 'start_date', name: "Datum"}
            },
            errors: {},
            examinations: []
        };

        this.renderExamination = this.renderExamination.bind(this);
        this.getUserExaminations = this.getUserExaminations.bind(this);
        this.change = this.change.bind(this);
    } 

    componentDidMount(){
       this.getUserExaminations();
    }

    getUserExaminations(){
        getUserExaminations({
            sort: this.state.data.sort.value
        }).then(response => {

            if(!response.ok) {
                return;
            }

            this.setState({
                ...this.data,
                examinations: response.data.examinations
            });
        });
    }


    renderExamination(){
        let exams = [];
        for (let i of this.state.examinations){
            exams.push(
                <div className="item">
                <div className="value">{this.dateToString(i.start_date)}</div>
                <div className="value">{i.doctor.first_name} {i.doctor.last_name}</div>
                <div className="value">{getExaminationTypeString(i.doctor.doctor.doctor_type)}</div>
                <div className="value">{i.doctor.doctor.clinic.name}</div>
                <div className="value">{getExaminationStatusString(i.status)}</div>
            </div>
            );
        }

        return exams;
    } 

    dateToString(date, format = 'DD-MM-YYYY hh:mm a') {
        return moment(date).format(format);
    }


    change(event){

        console.log(event);
        this.setState({
            data: update(this.state.data, { [event.target.name]: {$set: event.target.value} })
        });
       
        
        setTimeout(() => {
            this.getUserExaminations();
        }, 200);

    }

    

    render() {

        return (
            <div id="cartons">
                <h3>Istorija pregleda</h3> 
                <div className="sort">
                    <SelectControl
                        className='select'
                        options={ExaminationSortType}
                        onChange={ this.change }
                        name='sort'
                        nameKey = {'name'}
                        valueKey = {'value'}
                        selected={this.state.data.sort}
                    />
                </div>
                <div className="tabela">
                    <div className="header">
                        <div className="title">Datum</div>
                        <div className="title">Lekar</div>
                        <div className="title">Tip pregleda</div>
                        <div className="title">Klinika</div>
                        <div className="title">Status</div>
                    </div>

                    { this.renderExamination() }
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Examination));