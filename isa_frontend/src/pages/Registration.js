import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";
import { register } from "../services/UserService";
import { getClinics } from "../services/UserService";
import SelectControl from "../components/controls/SelectControl";



class Registration extends Page {

    validationList = {
        firstName: [ {type: Validators.REQUIRED } ],
        lastName: [ {type: Validators.REQUIRED } ],
        jmbg: [ {type: Validators.REQUIRED } ],
        insurance: [ {type: Validators.REQUIRED } ],
        email: [ {type: Validators.REQUIRED } ],
        dateOfBirth: [ {type: Validators.REQUIRED } ],
        address: [ {type: Validators.REQUIRED } ],
        city: [ {type: Validators.REQUIRED } ],
        country: [ {type: Validators.REQUIRED } ],
        phone: [ {type: Validators.REQUIRED } ],
        password: [ {type: Validators.REQUIRED } ],
        password1: [ {type: Validators.REQUIRED } ],
        clinic: [{type: Validators.REQUIRED }]

    };

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
            redirectUrl: "/home",
            clinicsOptions: []
        };

       this.keyPress = this.keyPress.bind(this);
    } 
    
    /* goToRegistrationPage() {
        this.props.history.push({
            pathname: "/registration"
       })
    }
    */


    componentDidMount(){
        getClinics({
        }).then(response => {

            if(!response.ok) {
                return;
            }

            let clinicsOptions = [];

            response.data.clinics.forEach(element => {
                clinicsOptions.push({value: element.id, name: element.name, id: element.id})
            })

            this.setState({
                ...this.state,
                clinicsOptions: clinicsOptions
            });
        });
    }

    keyPress(event) {

        if(event.key == 'Enter') {
            this.login()
        }
    } 

    registration(){
        if(!this.validate()) {
            return;
        }

        if(this.state.data.password != this.state.data.password1){
            alert("Password do not match!");
            return;
        }

        register(this.state.data).then(response => {

            if(!response.ok) {

                this.setError('email', 'Email exists');
                return;
            }

            alert("Successfull registration! Check your email!")

            /* this.props.history.push({
                 pathname: this.state.redirectUrl
            }) */
        });

    }


    render() {

        return (

            <div>
                <div className="container-login100">
                    <div className="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
                        <form className="login100-form validate-form">
                            <span className="login100-form-title p-b-37">
                                Sign Up
                            </span>

                            <div className="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">

                                <TextField
                                    placeholder="First name"
                                    //error={ hasError(errors, 'email') }
                                    //helperText={ getError(errors, 'email') }
                                    fullWidth
                                    autoFocus
                                    name='firstName'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.firstName }
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='lastName'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.lastName }
                                    placeholder="Last name"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='email'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.email }
                                    placeholder="Email"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='password'
                                    type='password'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.password }
                                    placeholder="Password"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='password1'
                                    type='password'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.password1 }
                                    placeholder="Password"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='jmbg'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.jmbg }
                                    placeholder="JMBG"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='dateOfBirth'
                                    type='date'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.dateOfBirth }
                                    label="Date of birth"
                                    InputLabelProps={{ shrink: true }}
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='address'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.address }
                                    placeholder="Address"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='city'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.city }
                                    placeholder="City"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='country'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.country }
                                    placeholder="Country"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='phone'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.phone }
                                    placeholder="Phone"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='insurance'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.insurance }
                                    placeholder="Insurance number"
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                                <SelectControl
                                    className='select'
                                    options={this.state.clinicsOptions}
                                    onChange={ this.changeData }
                                    name='clinic'
                                    nameKey = {'name'}
                                    valueKey = {'value'}
                                    placeholder={'Clinic'}
                                    selected={this.state.data.clinic}
                                />
                            </div>


                            <div className="container-login100-form-btn">
                                <Button className="login100-form-btn" onClick={ () => this.registration() }>
                                    Sign Up
                                </Button>
                            </div>

                        </form>

                        
                    </div>
                </div>
                <div id="dropDownSelect1"></div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Registration));