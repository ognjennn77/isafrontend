import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';
import SelectControl from "../components/controls/SelectControl";

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";
import DoctorType from "../constants/DoctorType";
import { getDoctors, 
        getClinics, 
        getExaminations,
        createExamination } from "../services/UserService";
import update from 'immutability-helper';
import CityOptions from "../constants/CityOptions";

class Schedule extends Page {

    validationList = {
        date: [ {type: Validators.REQUIRED } ],
    };


    constructor(props) {
        super(props);

        this.state = {
            data: {
                clinic: null,
                doctor: null,
                type: null
            },
            errors: {},
            doctorsOptions: [],
            examinations: []
        };

        this.getClinics = this.getClinics.bind(this);
        this.change = this.change.bind(this);
        this.getDoctors = this.getDoctors.bind(this);
        this.changeCity = this.changeCity.bind(this);
        this.getExaminations = this.getExaminations.bind(this);
    } 

    componentDidMount(){
       
        this.getDoctors();
        this.getClinics();
    }

    getClinics() {
        getClinics({
            city: this.state.data.city ? this.state.data.city.value : null
        }).then(response => {

            if(!response.ok) {
                return;
            }

            let clinicsOptions = [];

            response.data.clinics.forEach(element => {
                clinicsOptions.push({value: element.id, name: element.name, id: element.id})
            })

            this.setState({
                ...this.state,
                clinicsOptions: clinicsOptions
            });
        });
    }

    getDoctors(){
        getDoctors({
            clinic: this.state.data.clinic ? this.state.data.clinic.id : null,
            type: this.state.data.type ? this.state.data.type.value : null,
            city: this.state.data.city ? this.state.data.city.value : null
        }).then(response => {

            if(!response.ok) {
                return;
            }

            let doctorsOptions = [];

            response.data.doctors.forEach(element => {

                doctorsOptions.push({value: element.id, name: element.first_name + " " + element.last_name, id: element.id})
            });

            console.log(doctorsOptions);

            this.setState({
                ...this.state,
                doctors: response.data.doctors,
                doctorsOptions: doctorsOptions
            })
        });
    }


    change(event){
        this.setState({
            data: update(this.state.data, { [event.target.name]: {$set: event.target.value} })
        });
       
        setTimeout(() => {
            this.state.data.doctor = null;
            this.getDoctors();
        }, 200);

    }

    changeCity(event){
        this.setState({
            data: update(this.state.data, { [event.target.name]: {$set: event.target.value} })
        });
       
        setTimeout(() => {
            this.state.data.doctor = null;
            this.state.data.clinic = null;
            this.getClinics();
            this.getDoctors();
        }, 500);
    }

    getExaminations() {

        if(!this.validate()) {
            alert("Unesite datum kada zelite zakazati vas termin!");
            return;
        }

        getExaminations({
            clinic: this.state.data.clinic ? this.state.data.clinic.id : null,
            type: this.state.data.type ? this.state.data.type.value : null,
            city: this.state.data.city ? this.state.data.city.value : null,
            doctor: this.state.data.doctor ? this.state.data.doctor.id : null,
            date: this.state.data.date
        }).then(response => {

            if(!response.ok) {
                return;
            }

            this.setState({
                ...this.data,
                examinations: response.data.examinations
            })
        });
    }


    renderExamination(){
        let exams = [];
        for (let i of this.state.examinations){
            exams.push(
                <div className="exam">
                    <div className="item">{i.first_name} {i.last_name}</div>
                    <div className="item">{i.doctor.clinic.name} </div>
                    <div className="item">{this.state.data.date}</div>
                    <div className="item"><Button onClick={() => this.createExamination(i.id)}>Zakazi</Button></div>
                </div>
            );
        }

        return exams;
    }

    createExamination(doctorId){
        createExamination({
            doctorId: doctorId,
            date: this.state.data.date
        }).then(response => {

            if(!response.ok) {
                return;
            }

            this.props.history.push({
                pathname: "/examinations"
            })  
        });
    }
    

    render() {



        return (
            <div id="schedule">
                <h3>Zakazite vas termin</h3>
                <div className="search"> 
                    <div className="type"> 
                        <SelectControl
                            className='select'
                            options={DoctorType}
                            onChange={ this.change }
                            name='type'
                            nameKey = {'name'}
                            valueKey = {'value'}
                            selected={this.state.data.type}
                            placeholder={'Tip pregleda'}
                        />
                    </div>
                    <div className="date">
                        <TextField
                            fullWidth
                            name='date'
                            type='datetime-local'
                            onChange={ this.changeData }
                            //onKeyPress={ this.keyPress }
                            margin="normal"
                            value={ this.state.data.date }
                            label="Datum"
                            InputLabelProps={{ shrink: true }}
                        />
                    </div>
                    <div className="date">
                        <SelectControl
                            className='select'
                            options={this.state.clinicsOptions}
                            onChange={ this.change }
                            name='clinic'
                            nameKey = {'name'}
                            valueKey = {'value'}
                            selected={this.state.data.clinic}
                            placeholder={'Klinika'}
                        />
                    </div>
                    <div className="date">
                        <SelectControl
                            className='select'
                            options={this.state.doctorsOptions}
                            onChange={ this.changeData }
                            name='doctor'
                            nameKey = {'name'}
                            valueKey = {'value'}
                            placeholder={'Doktor'}
                            selected={this.state.data.doctor}
                        />
                    </div>
                    <div className="date">
                        <SelectControl
                            className='select'
                            options={CityOptions}
                            onChange={ this.changeCity }
                            name='city'
                            nameKey = {'name'}
                            valueKey = {'value'}
                            placeholder={'Grad'}
                            selected={this.state.data.city}
                        />
                    </div>
                    <div className="date">
                        <Button onClick={() => this.getExaminations()}>
                            Trazi
                        </Button>
                    </div>

                </div>
                { this.renderExamination() }
            
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Schedule));