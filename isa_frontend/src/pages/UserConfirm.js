import React from 'react'
import Page from "../common/Page";
import {withRouter} from 'react-router-dom';
import { confirmUser } from "../services/UserService";

class UserConfirm extends Page {

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
        };
    }

    componentDidMount() {

        let token = this.getSearchParam('token');

        if(!token) {
            this.props.history.push({
                pathname: '/'
            });

            return;
        }

        confirmUser({ token: token }).then(response => {

            if(!response.ok) {
                return;
            }

            this.props.history.push({
                pathname: '/'
            });
        });
    }

    render() {

        return (

            <div>

            </div>
        );
    }
}

export default withRouter(UserConfirm);