import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";



class HomePatient extends Page {

    validationList = {
        email: [ {type: Validators.REQUIRED } ],
        password: [ {type: Validators.REQUIRED } ]
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
        };
    } 
    

    goToExaminations(){
        this.props.history.push({
            pathname: "/examinations"
        })  
    }

    goToSchedule(){
        this.props.history.push({
            pathname: "/schedule"
        })  
    }

    goToCartons(){
        this.props.history.push({
            pathname: "/cartons"
        })  
    }

    render() {

        return (

            <div id="home-patient">
                <div className="part2" onClick={ () => this.goToCartons() }>
                    <h3>Karton</h3>
                </div>
                <div className="part1" onClick={ () => this.goToExaminations() }>
                    <h3>Istorija pregleda</h3>
                </div>
                <div className="part3" onClick={ () => this.goToSchedule() }>
                    <h3>Zakazivanje pregleda</h3>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomePatient));