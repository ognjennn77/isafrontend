import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";



class Cartons extends Page {

    validationList = {
        email: [ {type: Validators.REQUIRED } ],
        password: [ {type: Validators.REQUIRED } ]
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
        };
    } 
    

    

    render() {

        return (
            <div id="cartons">
                <h3>Vas karton</h3>
                <div className="tabela">
                    <div className="header">
                        <div className="title">Tip pregleda</div>
                        <div className="title">Lekar</div>
                        <div className="title">Vreme pregleda</div>
                        <div className="title">Dijagnoza</div>
                        <div className="title">Medikamenti</div>
                    </div>

                    <div className="item">
                        <div className="value">Tip pregleda</div>
                        <div className="value">Lekar</div>
                        <div className="value">Vreme pregleda</div>
                        <div className="value">Dijagnoza</div>
                        <div className="value">Medikamenti</div>
                    </div>

                    <div className="item">
                        <div className="value">Tip pregleda</div>
                        <div className="value">Lekar</div>
                        <div className="value">Vreme pregleda</div>
                        <div className="value">Dijagnoza</div>
                        <div className="value">Medikamenti</div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Cartons));