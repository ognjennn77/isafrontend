import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";
import { register } from "../services/UserService";
import { getClinics, getUser } from "../services/UserService";
import SelectControl from "../components/controls/SelectControl";
import moment from "moment";



class MyProfile extends Page {

    validationList = {
        firstName: [ {type: Validators.REQUIRED } ],
        lastName: [ {type: Validators.REQUIRED } ],
        jmbg: [ {type: Validators.REQUIRED } ],
        insurance: [ {type: Validators.REQUIRED } ],
        email: [ {type: Validators.REQUIRED } ],
        dateOfBirth: [ {type: Validators.REQUIRED } ],
        address: [ {type: Validators.REQUIRED } ],
        city: [ {type: Validators.REQUIRED } ],
        country: [ {type: Validators.REQUIRED } ],
        phone: [ {type: Validators.REQUIRED } ],
        password: [ {type: Validators.REQUIRED } ],
        password1: [ {type: Validators.REQUIRED } ],
        clinic: [{type: Validators.REQUIRED }]

    };

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
            redirectUrl: "/home",
            clinicsOptions: []
        };

       this.keyPress = this.keyPress.bind(this);
    } 
    
    /* goToRegistrationPage() {
        this.props.history.push({
            pathname: "/registration"
       })
    }
    */


    componentDidMount(){
        getUser().then(response => {

            if(!response.ok) {
                return;
            }

            console.log(response);

            console.log(this.stringToDate(response.data.user.birth_date));

            this.setState({
                ...this.state,
                data: {
                    firstName: response.data.user.first_name,
                    lastName: response.data.user.last_name,
                    jmbg: response.data.user.jmbg,
                    city: response.data.user.city,
                    country: response.data.user.country,
                    phone: response.data.user.phone,
                    insurance: response.data.user.insurance,
                    address: response.data.user.address,
                    email: response.data.user.email,
                    dateOfBirth: this.stringToDate(response.data.user.birth_date),
                }

            });
        });
    }

    keyPress(event) {

        if(event.key == 'Enter') {
            this.login()
        }
    } 

    registration(){
        if(!this.validate()) {
            return;
        }

        if(this.state.data.password != this.state.data.password1){
            alert("Password do not match!");
            return;
        }

        register(this.state.data).then(response => {

            if(!response.ok) {

                this.setError('email', 'Email exists');
                return;
            }

            alert("Successfull registration! Check your email!")

            /* this.props.history.push({
                 pathname: this.state.redirectUrl
            }) */
        });

    }

    stringToDate(date, format = 'YYYY-MM-DD') {
        return moment(date, format);
    }


    render() {

        return (

            <div>
                <form id="my-profile">
                    <span className="login100-form-title p-b-37">
                        My profile
                    </span>

                    <div className="profile-row">

                            <div className="field" data-validate="Enter username or email">

                                <TextField
                                    placeholder="First name"
                                    //error={ hasError(errors, 'email') }
                                    //helperText={ getError(errors, 'email') }
                                    fullWidth
                                    autoFocus
                                    name='firstName'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.firstName }
                                    label="First name"
                                    InputLabelProps={{ shrink: true }}
                                    InputProps={{
                                        readOnly: true,
                                      }}
                                />
                            </div>

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='lastName'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.lastName }
                                    label="Last name"
                                    InputLabelProps={{ shrink: true}}
                                    InputProps={{
                                        readOnly: true,
                                      }}
                                />
                            </div>

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='email'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.email }
                                    label="Email"
                                    InputLabelProps={{ shrink: true }}
                                    InputProps={{
                                        readOnly: true,
                                      }}
                                />
                            </div>
                        </div>

                        <div className="profile-row">

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='phone'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.phone }
                                    label="Phone"
                                    InputLabelProps={{ shrink: true}}
                                    InputProps={{
                                        readOnly: true,
                                      }}
                                />
                            </div>

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='jmbg'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.jmbg }
                                    label="JMBG"
                                    InputLabelProps={{ shrink: true}}
                                    InputProps={{
                                        readOnly: true,
                                      }}
                                />
                            </div>

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='insurance'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.insurance }
                                    label="Insurance number"
                                    InputLabelProps={{ shrink: true}}
                                    InputProps={{
                                        readOnly: true,
                                      }}
                                />
                            </div>
                        </div>

                        <div className="profile-row">

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='address'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.address }
                                    label="Address"
                                    InputLabelProps={{ shrink: true, readOnly: true, }}
                                />
                            </div>

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='city'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.city }
                                    label="City"
                                    InputLabelProps={{ shrink: true, readOnly: true, }}
                                />
                            </div>

                            <div className="field" data-validate = "Enter password">
                                <TextField
                                    //error={ hasError(errors, 'password') }
                                    //helperText={ getError(errors, 'password') }
                                    fullWidth
                                    name='country'
                                    type='text'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.country }
                                    label="Country"
                                    InputLabelProps={{ shrink: true, readOnly: true,}}
                                />
                            </div>

                        </div>

                </form>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyProfile));