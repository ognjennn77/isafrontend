import React from 'react'
import Page from "../common/Page";
import {withRouter} from 'react-router-dom';
import { confirmEximantion } from "../services/UserService";

class ExaminationConfirm extends Page {

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
        };
    }

    componentDidMount() {

        let id = this.getSearchParam('id');

        if(!id) {
            this.props.history.push({
                pathname: '/examinations'
            });

            return;
        }

        confirmEximantion({ id: id }).then(response => {

            if(!response.ok) {
                return;
            }

            this.props.history.push({
                pathname: '/examinations'
            });
        });
    }

    render() {

        return (

            <div>

            </div>
        );
    }
}

export default withRouter(ExaminationConfirm);