import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";



class Home extends Page {

    validationList = {
        email: [ {type: Validators.REQUIRED } ],
        password: [ {type: Validators.REQUIRED } ]
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
            redirectUrl: "/home"
        };

       this.keyPress = this.keyPress.bind(this);
    } 
    
    /* goToRegistrationPage() {
        this.props.history.push({
            pathname: "/registration"
       })
    }
    */

    keyPress(event) {

        if(event.key == 'Enter') {
            this.login()
        }
    } 

    login(){

        login(this.state.data.email, this.state.data.password).then(response => {

            if(!response.ok) {

                this.setError('email', 'Wrong Credentials');
                return;
            }

            this.props.login(response.data.user);


            this.props.history.push({
                pathname: this.state.redirectUrl
            });
        });
    }

    render() {

        return (

            <div>
                <div className="container-login100">
                    <div className="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
                        <form className="login100-form validate-form">
                            <span className="login100-form-title p-b-37">
                                Sign In
                            </span>

                            <div className="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">
                                {/* <input className="input100" type="text" name="username" placeholder="username or email"/>
                                <span className="focus-input100"></span> */}

                                <TextField
                                    placeholder="Email"
                                    //error={ hasError(errors, 'email') }
                                    //helperText={ getError(errors, 'email') }
                                    fullWidth
                                    autoFocus
                                    name='email'
                                    onChange={ this.changeData }
                                    onKeyPress={ this.keyPress }
                                    margin="normal"
                                    value={ this.state.data.email }
                                />
                            </div>

                            <div className="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
                            <TextField
                                //error={ hasError(errors, 'password') }
                                //helperText={ getError(errors, 'password') }
                                fullWidth
                                name='password'
                                type='password'
                                onChange={ this.changeData }
                                onKeyPress={ this.keyPress }
                                margin="normal"
                                value={ this.state.data.password }
                                placeholder="Password"
                            />
                            </div>

                            <div className="container-login100-form-btn">
                                <Button className="login100-form-btn" onClick={() => this.login()}>
                                    Sign In
                                </Button>
                            </div>


                            <div className="text-center" style={{marginTop: "50px"}}>
                                <a href="/registration" className="txt2 hov1">
                                    Sign Up
                                </a>
                            </div>
                        </form>

                        
                    </div>
                </div>
                <div id="dropDownSelect1"></div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));