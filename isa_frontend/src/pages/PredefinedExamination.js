import React from 'react';
import Page from "../common/Page";
import Button from '@material-ui/core/Button';
import { bindActionCreators } from "redux";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import TextField from '@material-ui/core/TextField';

import {login} from "../base/OAuth";
import * as Actions from "../actions/Actions";
import Validators from "../constants/ValidatorTypes";
import { getPredefinedExamination, changeExaminationStatus } from "../services/UserService";
import { getExaminationTypeString } from "../constants/ExaminationType";
import { getExaminationStatusString } from "../constants/ExaminationStatus";
import moment from "moment";


class PredefinedExamination extends Page {

    validationList = {
        email: [ {type: Validators.REQUIRED } ],
        password: [ {type: Validators.REQUIRED } ]
    };

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {},
            examinations: []
        };

        this.renderExamination = this.renderExamination.bind(this);
        this.getExaminations = this.getExaminations.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
    } 

    componentDidMount(){
        this.getExaminations();
    }

    getExaminations(){
        getPredefinedExamination().then(response => {

            if(!response.ok) {
                return;
            }

            this.setState({
                ...this.data,
                examinations: response.data.examinations
            });
        });
    }

    changeStatus(id, version){
        changeExaminationStatus({ examinationId: id, version: version });

        setTimeout(() => {
            this.getExaminations();
        }, 500);
    }


    renderExamination(){
        let exams = [];
        for (let i of this.state.examinations){

            console.log(i);
            exams.push(
                <div className="item">
                <div className="value">{this.dateToString(i.start_date)}</div>
                <div className="value">{i.doctor.first_name} {i.doctor.last_name}</div>
                <div className="value">{getExaminationTypeString(i.doctor.doctor.doctor_type)}</div>
                <div className="value">{i.doctor.doctor.clinic.name}</div>
                <div className="value">
                {
                    i.status == 0 && 
                    <Button onClick={() => this.changeStatus(i.id, i.version)}>Potvrdi dolazak</Button>
                }
                {
                    i.status == 1 && 
                    getExaminationStatusString(i.status)
                }
                </div>
            </div>
            );
        }

        return exams;
    } 

    dateToString(date, format = 'DD-MM-YYYY hh:mm a') {
        return moment(date).format(format);
    }

    

    render() {

        return (
            <div id="cartons">
                <h3>Istorija pregleda</h3>
                <div className="tabela">
                    <div className="header">
                        <div className="title">Datum</div>
                        <div className="title">Lekar</div>
                        <div className="title">Tip pregleda</div>
                        <div className="title">Klinika</div>
                        <div className="title">Status</div>
                    </div>

                    { this.renderExamination() }
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        login: Actions.login
    }, dispatch);
}

function mapStateToProps({ authReducers })
{
    return { auth: authReducers };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PredefinedExamination));