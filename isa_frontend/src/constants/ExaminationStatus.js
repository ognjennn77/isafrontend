import strings from "../localization";

const ExaminationStatus = [
    {
        value: 0,
        name: 'PENDING'
    },
    {
        value: 1,
        name: 'ACCEPTED'
    }
];

export function getExaminationStatusString(type) {

    switch(type) {
        case 0: return 'CEKA SE POTVRTDA';
        case 1: return 'POTVRDJEN';
    }
}

export default ExaminationStatus;