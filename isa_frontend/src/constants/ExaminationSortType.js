import strings from "../localization";

const ExaminationSortType = [
    {
        value: 'start_date',
        name: 'Datum'
    },
    {
        value: 'type',
        name: 'Tip'
    },
    {
        value: 'status',
        name: 'Status'
    }
];

export default ExaminationSortType;