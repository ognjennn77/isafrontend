const CityOptions = [
    {
        value: "Beograd",
        name: 'Beograd'
    },
    {
        value: "Novi Sad",
        name: 'Novi Sad'
    }
];

export default CityOptions;