const DoctorType = [
    {
        value: 0,
        name: 'Stomatolog'
    },
    {
        value: 1,
        name: 'Pedijatar'
    },
    {
        value: 2,
        name: 'Opsta praksa'
    },
    {
        value: 3,
        name: 'Kardiolog'
    },
    {
        value: 4,
        name: 'Fizijatar'
    },
    {
        value: 5,
        name: 'Urolog'
    },
    {
        value: 6,
        name: 'Ginekoog'
    }
];

export default DoctorType;