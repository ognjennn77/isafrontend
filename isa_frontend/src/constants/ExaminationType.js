import strings from "../localization";

const ExaminationType = [
    {
        value: 0,
        name: 'STOMATOLOG'
    },
    {
        value: 1,
        name: 'PEDIJATAR'
    },
    {
        value: 2,
        name: 'OPSTA_PRAKSA'
    },
    {
        value: 3,
        name: 'KARDIOLOG'
    },
    {
        value: 4,
        name: 'FIZIJATAR'
    },
    {
        value: 5,
        name: 'UROLOG'
    },
    {
        value: 6,
        name: 'GINEKOLOG'
    }
];

export function getExaminationTypeString(type) {

    switch(type) {
        case 0: return 'STOMATOLOG';
        case 1: return 'PEDIJATAR';
        case 2: return 'OPSTA PRAKSA';
        case 3: return 'KARDIOLOG';
        case 4: return 'FIZIJATAR';
        case 5: return 'UROLOG';
        case 6: return 'GINEKOLOG';
    }
}

export default ExaminationType;