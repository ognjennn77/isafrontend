import React, {Component} from 'react'
import {validate, isFormValid} from '../functions/Validation';
import BaseComponent from "../common/BaseComponent";
import update from 'immutability-helper';

class FormComponent extends BaseComponent {

    validationList = {};

    constructor(props) {
        super(props);

        this.state = {
            data: {},
            errors: {}

        };

        this.changeData = this.changeData.bind(this);
        this.changeDataSelector = this.changeDataSelector.bind(this);
        this.changeCheckBox = this.changeCheckBox.bind(this);
        this.validate = this.validate.bind(this);
    }

    changeData(event) {

        this.setState({
            data: update(this.state.data, { [event.target.name]: {$set: event.target.value} })
        });
    }

    changeCheckBox(event) {

        const field = event.target.name;
        const data = this.state.data;
        data[field] = !data[field];

        this.setState({
            data
        });
    }

    isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    isFloat(n) {
        return !isNaN(parseFloat(n));
    }

    isInt(n) {
        return isFinite(n)
    }

    validate () {

        let errors = validate(this.state.data, this.validationList);

        this.setState({errors});

        return isFormValid(errors);
    }

    setError(key, value) {
        this.setState({
            errors: update(this.state.errors, { [key]: {$set: [ { message: value} ]} })
        });
    }


    changeDataSelector(event, data = 'data') {
        this.setState({
            data: update(this.state.data, { [event.target.name]: {$set: event.target.value.id} })
        });
    }

    changeCheckBox(event) {
        const field = event.target.name;
        const data = this.state.data;
        data[field] = !data[field];

        this.setState({
            data
        });
    }

    changeCheckboxPreference(event){
        event.stopPropagation();
        var statePreferenceOptions = this.state.data.preferenceOptions;
        var entered = false;

        for (let index = 0; index < statePreferenceOptions.length; index++) {
            if(statePreferenceOptions[index] === +event.target.value) {
                statePreferenceOptions.splice(index, 1);
                entered = true;

                this.setState({
                    data : {
                        ...this.state.data,
                        preferenceOptions: statePreferenceOptions

                    }
                });
            }
        }

        if(!entered){
            this.setState({
                data : {
                    ...this.state.data,
                    preferenceOptions: [
                        ...this.state.data.preferenceOptions,
                        +event.target.value
                    ]
                }
            });
        }


    }
}

export default FormComponent;