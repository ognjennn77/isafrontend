import * as Actions from '../actions/Actions';

const initialState = {
    loader: false,
    loaderCount: 0
};

const siteDataReducers = (state = initialState, action) => {

    switch (action.type) {

        case Actions.TOGGLE_LOADER:

            let loaderCount = action.loader ? state.loaderCount + 1 : state.loaderCount - 1;

            return {
                ...state,
                loaderCount: loaderCount,
                loader: loaderCount > 0
            };
        case Actions.FORCE_HIDE_LOADER:
            return {
                ...state,
                loaderCount: 0,
                loader: false
            };

        case Actions.GET_GIRL_ID:
            return {
                ...state,
                girlId : action.girlId,
                event : action.event,
            };
        case Actions.CLEAR_GIRL_ID:
            return {
                ...state,
                girlId : undefined,
                event : undefined
            };
        default: return state;
    }
};

export default siteDataReducers