import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Header from "../components/Header";
import * as Actions from "../actions/Actions";
import BaseComponent from "../common/BaseComponent";

class BaseLayout extends BaseComponent {

    constructor(props) {
        super(props);

    }

    hideHeader(){
        let header = true;

        if(this.props.location.pathname == "/" || this.props.location.pathname == "/registration"){
            header = false;
        }

        return header;
    }



    render() {

        const {children} = this.props;

        return (
            <React.Fragment>

                {
                    this.hideHeader() &&
                    <Header/>
                }
                { children }
            </React.Fragment>
        )
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators({
        clearGirlId: Actions.clearGirlId
    }, dispatch);
}

function mapStateToProps({ authReducers, siteDataReducers })
{
    return {
        event : siteDataReducers.event,
        loader: siteDataReducers.loader
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BaseLayout));
