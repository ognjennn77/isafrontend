# **Projekat ISA 2019/20 FrontEnd**
Frontend je realizovan uz pomoc react tehnologije. Da bi se Frontend uspesno pokrenuo potrebno je imati sledece programe:

- Node.js v12.16.3

### **Preuzimanje projekta**

Kako biste uspjesno preuzeli projekat potrebno je preuzeti projekat komandom:
- **git clone https://gitlab.com/ognjennn77/isafrontend.git**
    
### **Pokretanje projekta**

Za pokretanje je potrebno otvoriti projekat u nekom od okruzenja,
preporucljivo bi bilo otvoriti ga koristeci Visual Studio Code.
Nakon uspjesno otvorenog projekta potrebno je u terminalu ukucati sledece komande:
        
- npm install
- npm start


